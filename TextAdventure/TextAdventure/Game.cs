﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace TextAdventure
{
    public class Game
    {
        private String gameName = "Red vs Blue Knowledge Quiz";   // a String initialized to empty
        private bool shouldExit = false; // Create a bool (true/false logical operator) indicating if we should exit the game
        private int score = 0;
        private int namesComplete = 0;

        // Constructor for the Game class
        // This is the code that is called whenever an instance of Game is created
        public Game()
        {

        }

        // Function (segment of logic) called from Program.cs when the application starts
        public void Setup()
        {
            // Storing the String "My First Game" (without quotes) in the variable named "gameName"
            gameName = "Red vs Blue Knowledge Quiz";
        }

        public void Begin()
        {
            // Print various lines of strings to the Console window
            TypeLine("Arrancando Unit-417-c <Lopez>**************************");
            TypeLine("Cambiar el texto del Espanol al Ingles***********************");
            TypeLine("Accesing quiz mode*****************");
            TypeLine("I bet you think you know something about Red vs Blue huh? Well have fun with  " + gameName);    // the + operator concatenate two strings (combines them)

            // Loop until shouldExit is true
            while (shouldExit == false)
            {
                if (namesComplete >= 4)
                {
                    Console.Write("Wow...Congrats you won!-_-");  //You win! exit
                    Console.Write("Score= 15-16 (Great Job-_-)");
                    Console.Write("Score= 12-13 (Ok not bad)");
                    Console.Write("Score= >12   (Pathetic you don't know anything about this show and are a gigantic fail.)");
                }
                else
                {
                    Console.Write("Pick a character from Red vs Blue: ");  // Write a partial line
                    String command = Console.ReadLine();    // Read a line from Console (when Enter is hit) and store it in command

                    HandleCommand(command);
                }
            }
        }

        public void TypeLine(String str)
        {
            int length = str.Length;
            for (int i = 0; i < length; i++)
            {
                Console.Write(str.Substring(i, 1));
                System.Threading.Thread.Sleep(40);
            }
            Console.WriteLine();
        }

        public void HandleCommand(String command)
        {
            // Enforce that the String is lowercase
            String answer;
            command = command.ToLower();

            if (command == "grif")
            {
                TypeLine("accesing Grif(orange idiot)");
                Console.Write("What is grif's favorite activity?");
                answer = Console.ReadLine().ToLower();
                if (answer == "napping")
                {
                    Console.WriteLine("right!");
                    score += 1;
                }
                Console.Write("What is Grif's first name?");
                answer = Console.ReadLine().ToLower();
                if (answer == "dexter")
                {
                    Console.WriteLine("Right!");
                    score += 1;
                }
                Console.Write("OK......... What is his specialty on red team?");
                answer = Console.ReadLine().ToLower();
                if (answer == "driving")
                {
                    Console.WriteLine("Right!");
                    score += 1;
                }
                Console.Write("Just about finished with Grif. Well then when fighting Agent Tex how many times does grif get hit in the crotch?");
                answer = Console.ReadLine().ToLower();
                if (answer == "six")
                {
                    Console.WriteLine("Right!");
                    score += 1;
                }
                namesComplete++;
                Console.WriteLine("Well...... That was Grif. But do you know the others?");
            }
            else if (command == "caboose")
            {
                Console.Write("Ok. What is Caboose's first name?");
                answer = Console.ReadLine().ToLower();
                if (answer == "michael")
                {
                   Console.WriteLine("Right!");
                   score += 1;
                }
                {
                    Console.Write("It's actually Michael J. bimbo, but whatever it counts. What does Caboose LOVE?");
                    answer = Console.ReadLine().ToLower();
                    if (answer == "buttons")
                    {
                        Console.WriteLine("Right!");
                        score += 1;
                    }
                    Console.Write("Well then, What does Caboose visit in season 9?");
                    answer = Console.ReadLine().ToLower();
                    if (answer == "Halo Reach Campaign") ;
                    {
                        Console.WriteLine("Right!");
                        score += 1;
                    }
                    Console.Write("What color is Church in Caboose's mind?");
                    answer = Console.ReadLine().ToLower();
                    if (answer == "yellow") ;
                    {
                        Console.WriteLine("Right!");
                        score += 1;
                    }
                }
            }
            else if (command == "sarge")
            {
                Console.WriteLine("only wants to die one way,.... after grif.");
            }
            else if (command == "simmons")
            {
                Console.Write("What is Simmons' first name?");
                answer = Console.ReadLine().ToLower();
                if (answer == "richard")
                {
                }
            }

        }

    }
}
